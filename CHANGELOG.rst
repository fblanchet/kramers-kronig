ChangeLog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_.

.. _Keep a Changelog: https://keepachangelog.com/
.. _Semantic Versioning: https://semver.org/

Unreleased
----------

Added
^^^^^

Change
^^^^^^

Deprecated
^^^^^^^^^^

Removed
^^^^^^^

Fixed
^^^^^

[1.0.0]
-------

Added
^^^^^

* Kramers-Kronig algorithm through Fourier space
