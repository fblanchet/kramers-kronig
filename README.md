## Introduction

Python package to compute Kramers-Kronig relations through Fourier transforms.

See more details at [ReadTheDocs.io](https://kramers-kronig.readthedocs.io/en/latest/).
