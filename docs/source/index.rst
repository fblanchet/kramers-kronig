.. mdinclude:: ../../README.md

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Reference:

   self
   api
   changelog
   license
